﻿using Microsoft.Identity.Client;
using Microsoft.PowerBI.Api;
using Microsoft.PowerBI.Api.Models;
using Microsoft.Rest;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PowerBI_Embed.Models;
using PowerBI_Embed.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PowerBI_Embed.Controllers
{
    public class HomeController : Controller
    {
        private readonly IEmbedService m_embedService;

        public HomeController()
        {
            m_embedService = new EmbedService();
        }

        public ActionResult Index()
        {
            Session.Remove("username");
            Session.Remove("password");
            return View();
        }

        //Action to return list of reports
        [HttpGet]
        public JObject Login(string UserName, string Password)
        {
            JObject json = JObject.Parse("{\"status\":\"\"}");

            if (UserName.ToLower() == "ssapkal@dealersocket.com" && Password == "123")
            {
                Session["username"] = UserName.ToLower();
                Session["password"] = Password;
            }
            else if (UserName.ToLower() == "karolkar@dealersocket.com" && Password == "123")
            {
                Session["username"] = UserName.ToLower();
                Session["password"] = Password;
            }
            else if (UserName.ToLower() == "brajan@dealersocket.com" && Password == "123")
            {
                Session["username"] = UserName.ToLower();
                Session["password"] = Password;
            }
            else if (UserName.ToLower() == "rgandhi@dealersocket.com" && Password == "123")
            {
                Session["username"] = UserName.ToLower();
                Session["password"] = Password;
            }
            else if (UserName.ToLower() == "jpatel@dealersocket.com" && Password == "123")
            {
                Session["username"] = UserName.ToLower();
                Session["password"] = Password;
            }
            else if (UserName.ToLower() == "kpolepalle@dealersocket.com" && Password == "123")
            {
                Session["username"] = UserName.ToLower();
                Session["password"] = Password;
            }
            else if (UserName == "vvarigonda@dealersocket.com" && Password == "123")
            {
                Session["username"] = UserName.ToLower();
                Session["password"] = Password;
            }
            else if (UserName.ToLower() == "svellanki@dealersocket.com" && Password == "123")
            {
                Session["username"] = UserName.ToLower();
                Session["password"] = Password;
            }
            else if (UserName.ToLower() == "mhamling@dealersocket.com" && Password == "123")
            {
                Session["username"] = UserName.ToLower();
                Session["password"] = Password;
            }
            else if (UserName.ToLower() == "mvijay@dealersocket.com" && Password == "123")
            {
                Session["username"] = UserName.ToLower();
                Session["password"] = Password;
            }
            else if (UserName.ToLower() == "agayal@dealersocket.com" && Password == "123")
            {
                Session["username"] = UserName.ToLower();
                Session["password"] = Password;
            }
            else if (UserName.ToLower() == "asharma@dealersocket.com" && Password == "123")
            {
                Session["username"] = UserName.ToLower();
                Session["password"] = Password;
            }
            else if (UserName.ToLower() == "shamrell@dealersocket.com" && Password == "123")
            {
                Session["username"] = UserName.ToLower();
                Session["password"] = Password;
            }
            else
            {
                json["status"] = "failure";
                return json;
            }
            json["status"] = "success";

            return json;
        }

        //Action to return tokens for rendering the report
        public async Task<dynamic> RefreshToken(string reportName, string username, string roles)
        {
            if (Session["username"] != null && Session["password"] != null)
            {
                var embedResult = await m_embedService.EmbedReport(reportName, username, roles);
                string JSON = JsonConvert.SerializeObject(m_embedService.EmbedConfig);
                return JSON;
            }
            return RedirectToAction("Index");

        }

        public async Task<dynamic> GetToken()
        {
            if (Session["username"] != null && Session["password"] != null)
            {
                var data = "accesstoken";
                var reports = await m_embedService.GetReports(data);
                var response = JsonConvert.SerializeObject(reports);
                return response;
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
    

        //Action to return list of reports
        public async Task<dynamic> GetReports(string data = null)
        {
            //if (Session["username"] != null && Session["password"] != null)
            //{
            //    var reports = await m_embedService.GetReports();
            //    var a = JsonConvert.SerializeObject(reports);
            //    return a;
            //}
            string username = Session["username"].ToString();
            string password = Session["password"].ToString();

            if (!string.IsNullOrEmpty(data))
            {
                var reports = await m_embedService.GetReports(data);
                return JsonConvert.SerializeObject(reports);
            }
            else
            {

                //Michael to see only KPI reports
                if (username == "mhamling@dealersocket.com" && password == "123")
                {
                    var abc = "{\r\n    \"odata.context\": null,\r\n    \"value\": [\r\n      {\r\n        \"id\": \"ed86fe5d-4e3e-4bcf-a69a-c31bb74dd0e2\",\r\n        \"name\": \"KPI By Summary Group\",\r\n        \"datasetId\": \"957eca87-50ff-4fc8-9685-3b9dd41cc6da\",\r\n        \"Role\":\"Analyst\"\r\n\r\n      }\r\n    ]\r\n  }";
                    return abc;
                }

                //Keshave to see 2 reports
                else if (username == "kpolepalle@dealersocket.com" && password == "123")
                {
                    var abc = "{\r\n  \"odata.context\": null,\r\n  \"value\": [\r\n    {\r\n      \"id\": \"ed86fe5d-4e3e-4bcf-a69a-c31bb74dd0e2\",\r\n      \"name\": \"KPI By Summary Group\",\r\n      \"datasetId\": \"957eca87-50ff-4fc8-9685-3b9dd41cc6da\",\r\n      \"Role\":\"Analyst\"\r\n    },\r\n    {\r\n      \"id\": \"df3f03d0-e767-4356-8a83-1e7bf0830c58\",\r\n      \"name\": \"Response Time\",\r\n      \"datasetId\": \"957eca87-50ff-4fc8-9685-3b9dd41cc6da\",\r\n      \"Role\":\"Analyst\"\r\n    }\r\n  ]\r\n}";
                    return abc;
                }   
                
                //Karishma to see all reports to see 2 reports
                else if (username == "karolkar@dealersocket.com" && password == "123")
                {
                    var abc = "{\r\n    \"odata.context\": null,\r\n    \"value\": [\r\n      {\r\n        \"id\": \"ed86fe5d-4e3e-4bcf-a69a-c31bb74dd0e2\",\r\n        \"name\": \"KPI By Summary Group\",\r\n        \"datasetId\": \"957eca87-50ff-4fc8-9685-3b9dd41cc6da\",\r\n        \"Role\":\"Analyst\"\r\n      },      \r\n      {\r\n        \"id\": \"a0b3b6e3-8ed7-4993-be1a-d8c4bf417827\",\r\n        \"name\": \"KPI By Summary Group_MultiPage Report\",\r\n        \"datasetId\": \"8ada8809-1300-4866-8f20-054864df82d1\",\r\n        \"Role\":\"Analyst\"\r\n      },\r\n      {\r\n        \"id\": \"f7ccd945-6924-4189-9b6c-653947f154ff\",\r\n        \"name\": \"Report Usage Metrics Report\",\r\n        \"datasetId\": \"957eca87-50ff-4fc8-9685-3b9dd41cc6da\",\r\n        \"Role\":\"null\"\r\n      },\r\n      {\r\n        \"id\": \"df3f03d0-e767-4356-8a83-1e7bf0830c58\",\r\n        \"name\": \"Response Time\",\r\n        \"datasetId\": \"957eca87-50ff-4fc8-9685-3b9dd41cc6da\",\r\n        \"Role\":\"Analyst\"\r\n      }\r\n      \r\n    ]\r\n  }";
                    return abc;
                }

                //Rest to all see 3 reports
                else
                {
                    var abc = "{\r\n    \"odata.context\": null,\r\n    \"value\": [\r\n      {\r\n        \"id\": \"ed86fe5d-4e3e-4bcf-a69a-c31bb74dd0e2\",\r\n        \"name\": \"KPI By Summary Group\",\r\n        \"datasetId\": \"957eca87-50ff-4fc8-9685-3b9dd41cc6da\",\r\n        \"Role\":\"Analyst\"\r\n      },      \r\n      {\r\n        \"id\": \"f7ccd945-6924-4189-9b6c-653947f154ff\",\r\n        \"name\": \"Report Usage Metrics Report\",\r\n        \"datasetId\": \"957eca87-50ff-4fc8-9685-3b9dd41cc6da\",\r\n        \"Role\":\"null\"\r\n      },\r\n      {\r\n        \"id\": \"df3f03d0-e767-4356-8a83-1e7bf0830c58\",\r\n        \"name\": \"Response Time\",\r\n        \"datasetId\": \"957eca87-50ff-4fc8-9685-3b9dd41cc6da\",\r\n        \"Role\":\"Analyst\"\r\n      }\r\n      \r\n    ]\r\n  }";
                    return abc;
                }


            }






        }
    }
}
