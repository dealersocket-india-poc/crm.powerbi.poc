﻿
import React from 'react';
import ReactDOM from 'react-dom';
import NavBar from './react-components/navbar/navbar'
import LoginForm from './react-components/login/login'
import RenderReport from './react-components/report/report'

import { LandinPage, SideBar } from './react-components/landingPage/landingpage'


class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoggedIn: false,
            loginError: false
        }

    }

    validateLogin(userName, password) {

        let apiEndPoint = "Home/Login?username=" + userName + "&password=" + password;
        fetch(apiEndPoint)
            .then(res => res.json())
            .then(
                (result) => {
                    if (result.status === "success") {
                        sessionStorage.setItem('userName', userName);
                        sessionStorage.setItem('role', 'Analyst');

                        this.setState(
                            {
                                isLoggedIn: true
                            }
                        );

                    }
                    else {
                        this.setState(
                            {
                                isLoggedIn: false,
                                loginError: true
                            }

                        );
                    }
                },
                (error) => {
                    console.log("error in api");
                }
            )

    }


    render() {

        return (
            <div>


                {this.state.isLoggedIn ?
                    <div>
                        <NavBar></NavBar>

                        <SideBar />

                    </div>

                    //: <RenderReport />
                    : <LoginForm
                        onClick={(userName, Password) => this.validateLogin(userName, Password)}
                        loginError={this.state.loginError}
                    ></LoginForm>

                }



            </div>
        );
    }

}

ReactDOM.render(<App />, document.getElementById("root"));