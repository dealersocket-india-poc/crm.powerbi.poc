﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PowerBI_Embed
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
                
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                //defaults: new { controller = "Home", action = "EmbedReport", id = UrlParameter.Optional }
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }

            );

            routes.MapRoute(
                name: "Home",
                url: "home/{action}/{reportname}/{username}/{roles}",
                defaults: new { controller = "Home", action = "RefreshToken", reportname = UrlParameter.Optional, username = UrlParameter.Optional, roles = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "GetReports",
                url: "home/{action}/{data}",
                defaults: new { controller = "home", action = "GetReports", data = UrlParameter.Optional }
            ); 
            
            routes.MapRoute(
                 name: "GetToken",
                 url: "home/{action}",
                 defaults: new { controller = "home", action = "GetToken"}
             );

        }
    }
}

