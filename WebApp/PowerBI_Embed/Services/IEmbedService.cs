﻿using PowerBI_Embed.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PowerBI_Embed.Services
{
    public interface IEmbedService
    {
        EmbedConfig EmbedConfig { get; }
        Task<Object> GetReports(string data);
        Task<bool> EmbedReport(string reportName, string userName, string roles);
    }
}