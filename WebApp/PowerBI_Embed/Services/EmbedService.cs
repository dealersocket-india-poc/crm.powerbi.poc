﻿using Microsoft.Azure.KeyVault;
using Microsoft.Azure.KeyVault.Models;
using Microsoft.Azure.Services.AppAuthentication;
using Microsoft.Identity.Client;
using Microsoft.PowerBI.Api;
using Microsoft.PowerBI.Api.Models;
using Microsoft.Rest;
using PowerBI_Embed.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Security;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PowerBI_Embed.Services
{
    public class EmbedService : IEmbedService
    {
        private static readonly string AuthorityUrl = ConfigurationManager.AppSettings["authorityUrl"];
        private static readonly string ResourceUrl = ConfigurationManager.AppSettings["resourceUrl"];
        private static readonly string[] Scope = ConfigurationManager.AppSettings["scope"].Split(';');
        private static readonly string ApplicationId = ConfigurationManager.AppSettings["applicationId"];
        private static readonly string ApiUrl = ConfigurationManager.AppSettings["apiUrl"];
        private static readonly Guid WorkspaceId = GetParamGuid(ConfigurationManager.AppSettings["workspaceId"]);
        private static readonly string AppSecretURL = ConfigurationManager.AppSettings["appSecretUrl"];
        private static readonly string Tenant = ConfigurationManager.AppSettings["tenant"];
        private static readonly string ApplicationSecret = GetSecret(AppSecretURL).Result.ToString();

        public EmbedConfig EmbedConfig
        {
            get { return m_embedConfig; }
        }

        private EmbedConfig m_embedConfig;

        private TokenCredentials m_tokenCredentials;

        public string AccessToken = string.Empty;

        public EmbedService()
        {
            m_tokenCredentials = null;
            m_embedConfig = new EmbedConfig();
        }

        private static Guid GetParamGuid(string param)
        {
            Guid paramGuid = Guid.Empty;
            Guid.TryParse(param, out paramGuid);
            return paramGuid;
        }


        //Get list of reports to show in the sidebar
        public async Task<Object> GetReports(string data = null)
        {
            if (ApplicationSecret == null)
            {
                return false;
            }
            // Get token credentials for user
            var getCredentialsResult = await GetTokenCredentials();
            if (!getCredentialsResult)
            {
                // The error message set in GetTokenCredentials
                return false;
            }
            try
            {
                if (data.ToLower() != "accesstoken")
                {
                    // Create a Power BI Client object. It will be used to call Power BI APIs.
                    using (var client = new PowerBIClient(new Uri(ApiUrl), m_tokenCredentials))
                    {
                        // Get a list of reports.
                        var reports = await client.Reports.GetReportsInGroupAsync(WorkspaceId);
                        return reports;
                    }
                }
                else
                {
                    return AccessToken;
                }

            }
            catch (HttpOperationException)
            {
                return false;
            }
        }





        //Get list of tokens required to render the report
        public async Task<bool> EmbedReport(string reportName, string username, string roles)
        {

            if (ApplicationSecret == null)
            {
                return false;
            }

            // Get token credentials for user
            var getCredentialsResult = await GetTokenCredentials();
            if (!getCredentialsResult)
            {
                // The error message set in GetTokenCredentials
                return false;
            }

            try
            {
                // Create a Power BI Client object. It will be used to call Power BI APIs.
                using (var client = new PowerBIClient(new Uri(ApiUrl), m_tokenCredentials))
                {
                    // Get a list of reports.
                    var reports = await client.Reports.GetReportsInGroupAsync(WorkspaceId);

                    // No reports retrieved for the given workspace.
                    if (reports.Value.Count() == 0)
                    {
                        m_embedConfig.ErrorMessage = "No reports were found in the workspace";
                        return false;
                    }

                    Report report;
                    report = reports.Value.FirstOrDefault(r => r.Name == reportName);
                    

                    if (report == null)
                    {
                        m_embedConfig.ErrorMessage = "No report with the given ID was found in the workspace. Make sure that the report ID is valid.";
                        return false;
                    }

                    var datasets = await client.Datasets.GetDatasetInGroupAsync(WorkspaceId, report.DatasetId);
                    GenerateTokenRequest generateTokenRequestParameters = new GenerateTokenRequest();

                    if (datasets.IsEffectiveIdentityRequired == true && datasets.IsEffectiveIdentityRolesRequired == true)
                    {
                        m_embedConfig.IsEffectiveIdentityRequired = datasets.IsEffectiveIdentityRequired;
                        m_embedConfig.IsEffectiveIdentityRolesRequired = datasets.IsEffectiveIdentityRolesRequired;
                        if (!string.IsNullOrWhiteSpace(username))
                        {
                            // This is how you create embed token with effective identities
                            var rls = new EffectiveIdentity(username, new List<string> { report.DatasetId });
                            if (!string.IsNullOrWhiteSpace(roles))
                            {
                                var rolesList = new List<string>();
                                rolesList.AddRange(roles.Split(','));
                                rls.Roles = rolesList;
                            }
                            // Generate Embed Token with effective identities.
                            generateTokenRequestParameters = new GenerateTokenRequest(accessLevel: "edit", identities: new List<EffectiveIdentity> { rls });
                        }

                    }
                    else
                    {
                        // Generate Embed Token for reports without effective identities.
                        generateTokenRequestParameters = new GenerateTokenRequest(accessLevel: "edit");

                    }

                    var tokenResponse = await client.Reports.GenerateTokenInGroupAsync(WorkspaceId, report.Id, generateTokenRequestParameters);
                    if (tokenResponse == null)
                    {
                        m_embedConfig.ErrorMessage = "Failed to generate embed token.";
                        return false;
                    }

                    // Generate Embed Configuration.
                    m_embedConfig.EmbedToken = tokenResponse;
                    m_embedConfig.EmbedUrl = report.EmbedUrl;
                    m_embedConfig.Id = report.Id.ToString();
                }
            }
            catch (HttpOperationException exc)
            {
                m_embedConfig.ErrorMessage = string.Format("Status: {0} ({1})\r\nResponse: {2}\r\nRequestId: {3}", exc.Response.StatusCode, (int)exc.Response.StatusCode, exc.Response.Content, exc.Response.Headers["RequestId"].FirstOrDefault());
                return false;
            }

            return true;
        }

        /// <summary>
        /// Check if web.config embed parameters have valid values.
        /// </summary>
        /// <returns>Null if web.config parameters are valid, otherwise returns specific error string.</returns>
        private string GetWebConfigErrors()
        {
            // Application Id must have a value.
            if (string.IsNullOrWhiteSpace(ApplicationId))
            {
                return "ApplicationId is empty. please register your application as Native app in https://dev.powerbi.com/apps and fill client Id in web.config.";
            }

            // Application Id must be a Guid object.
            Guid result;
            if (!Guid.TryParse(ApplicationId, out result))
            {
                return "ApplicationId must be a Guid object. please register your application as Native app in https://dev.powerbi.com/apps and fill application Id in web.config.";
            }

            // Workspace Id must have a value.
            if (WorkspaceId == Guid.Empty)
            {
                return "WorkspaceId is empty or not a valid Guid. Please fill its Id correctly in web.config";
            }

            
            if (string.IsNullOrWhiteSpace(ApplicationSecret))
            {
                return "ApplicationSecret is empty. please register your application as Web app and fill appSecret in web.config.";
            }

            // Must fill tenant Id
            if (string.IsNullOrWhiteSpace(Tenant))
            {
                return "Invalid Tenant. Please fill Tenant ID in Tenant under web.config";
            }

            return null;
        }

        private async Task<AuthenticationResult> DoAuthentication()
        {
            AuthenticationResult authenticationResult = null;
            
                // For app only authentication, we need the specific tenant id in the authority url
                var tenantSpecificURL = AuthorityUrl.Replace("organizations", Tenant);

                IConfidentialClientApplication clientApp = ConfidentialClientApplicationBuilder
                                                                                .Create(ApplicationId)
                                                                                .WithClientSecret(ApplicationSecret)
                                                                                .WithAuthority(tenantSpecificURL)
                                                                                .Build();
                try
                {
                    authenticationResult = await clientApp.AcquireTokenForClient(Scope).ExecuteAsync();
                }
                catch (MsalException exc)
                {                    
                    throw;
                }
            

            return authenticationResult;
        }

        private async Task<bool> GetTokenCredentials()
        {
            // var result = new EmbedConfig { Username = username, Roles = roles };
            var error = GetWebConfigErrors();
            if (error != null)
            {
                m_embedConfig.ErrorMessage = error;
                return false;
            }

            // Authenticate using created credentials
            AuthenticationResult authenticationResult = null;
            try
            {
                authenticationResult = await DoAuthentication();
                AccessToken = authenticationResult.AccessToken;
            }
            catch (AggregateException exc)
            {
                m_embedConfig.ErrorMessage = exc.InnerException.Message;
                return false;
            }

            if (authenticationResult == null)
            {
                m_embedConfig.ErrorMessage = "Authentication Failed.";
                return false;
            }

            m_tokenCredentials = new TokenCredentials(authenticationResult.AccessToken, "Bearer");
            return true;
        }

        private static async Task<string> GetSecret(string KeyVaultURL)
        {
            AzureServiceTokenProvider azureServiceTokenProvider = new AzureServiceTokenProvider();
            KeyVaultClient client;
            SecretBundle secret;
            //string result = "Temp";
            client = new KeyVaultClient(new KeyVaultClient.AuthenticationCallback(azureServiceTokenProvider.KeyVaultTokenCallback));
            try
            {
                secret = await client.GetSecretAsync(String.Format(KeyVaultURL)).ConfigureAwait(false);
                if (secret == null)
                    throw new InvalidOperationException("Failed to obtain token");
                return secret.Value.ToString();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error occured while fetching data from KeyVault");
                Console.WriteLine(e.ToString());
                Console.WriteLine(e.InnerException);
                return null;
            }
        }
    }
}