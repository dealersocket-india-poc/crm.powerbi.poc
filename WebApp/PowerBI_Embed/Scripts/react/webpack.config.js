﻿ 

module.exports = {
    mode: 'production',
    context: __dirname, 
    entry: "./app.js",
    output: {
        path: __dirname + "/dist",
            filename: "bundle.js"
    },
    watch: true,    
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/, //excludes node_modules folder from being transpiled by babel. We do this because it's a waste of resources to do so.,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ['babel-preset-env','babel-preset-react']
                    }
                }
                
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.(jpg|png|gif|svg|pdf|ico)$/,
                use: [
                    
                    'file-loader', 'url-loader'
                  
                ]
            }
        ]
    }
}   