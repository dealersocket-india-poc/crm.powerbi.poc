﻿import React from 'react';
import './login.css'



class LoginForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: ""
        };
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    validateLogin() {


        this.setState({
            isLoggedIn: true
        });
    }

    render() {
        return (
            <header className="login-page">

                <div>
                    <img src="Scripts/react/react-components/login/LoginPageImage.png" className="loginPageImage" />
                </div>
                <div className="LoginForm ">
                    <span><img src="Scripts/react/react-components/login/logo-login.svg" className="brand-logo-login" alt="DealerSocket logo" /></span>
                    <span className="login-error">{this.props.loginError ? "Invalid username / password" : ""}</span>
                    <div className="usernameField">
                        <input
                            type="text"
                            name="username"
                            className="username"
                            placeholder="Username"
                            onChange={this.handleChange}
                            value={this.state.username}
                            onChange={this.handleInputChange}
                        />
                        <img src="Scripts/react/react-components/login/Username icon.svg" />
                    </div>
                    <div className="usernameField">
                        <input type="password" name="password" className="password" placeholder="Password"
                            value={this.state.password}
                            onChange={this.handleInputChange}
                        />
                        <img src="Scripts/react/react-components/login/restricted.svg" />
                    </div>
                    <div>
                        <span className="rememberMeSection">
                            <input type="checkbox" value="Remember me" className="rememberMeText" />
                            Remember me
                        </span>
                        <span className="fPassSection"> Forget Password? </span>
                    </div>
                    <button class="btn px-3 btn-primary submit-button" onClick={() => this.props.onClick(this.state.username, this.state.password)} href="#" role="button">
                        <div class="submitButtonText">SIGN IN</div>
                    </button>


                </div>

            </header>
        );
    }
}

export default LoginForm;