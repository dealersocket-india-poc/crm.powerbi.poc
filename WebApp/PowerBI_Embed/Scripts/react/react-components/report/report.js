﻿import React from 'react';
//import { Navbar } from 'react-bootstrap';
//import './report.css';
import './report.css';
import * as pbi from "powerbi-client";
import { log } from 'util';


class RenderReport extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentReport: this.props.report
        }
    }

    callPowerBI() {

        $('.sidebarReportList').addClass("disabled");
        $('#exportButton').addClass("disabled");
        $('#exportButton').parent().addClass("loadingPointer");

        $('#embedContainer')[0].innerHTML = "<div class='loader'></div>";

        let apiEndPoint = "/Home/RefreshToken/" + this.props.report + "/" + sessionStorage.getItem("userName") + "/" + sessionStorage.getItem("role");

        var currentReportName = this.props.report;

        var models = window['powerbi-client'].models
        var config = {
            type: 'report',
            tokenType: models.TokenType.Embed,
            accessToken: "",
            embedUrl: "",
            id: "",
            permissions: models.Permissions.All,
            settings: {
                filterPaneEnabled: true,
                navContentPaneEnabled: true
            }
        };

        // Get a reference to the embedded report HTML element
        var reportContainer = $('#embedContainer')[0];



        var request = fetch(apiEndPoint)
            .then(res => res.json())
            .then(
                (result) => {

                    // Read embed application token from Model
                    var accessToken = result.EmbedToken.token;
                    // Read embed URL from Model
                    var embedUrl = result.EmbedUrl;
                    // Read report Id from Model
                    var embedReportId = result.Id;

                    // Read timetoExpire from Model
                    var timetoExpire = result.EmbedToken.expiration;
                    // Get models. models contains enums that can be used.
                    var models = window['powerbi-client'].models;


                    var config = {
                        type: 'report',
                        tokenType: models.TokenType.Embed,
                        accessToken: accessToken,
                        embedUrl: embedUrl,
                        id: embedReportId,
                        permissions: models.Permissions.All,

                        settings: {
                            panes: {
                                filters: {
                                    visible: true
                                },
                                pageNavigation: {
                                    visible: true
                                }
                            }
                        }
                    };

                    if (currentReportName === "KPI By Summary Group") {
                        config.viewMode = models.ViewMode.Edit;
                    }

                    // Get a reference to the embedded report HTML element
                    var reportContainer = $('#embedContainer')[0];
                    window.report = powerbi.embed(reportContainer, config);
                    $('.sidebarReportList').removeClass("disabled");
                    $('#exportButton').removeClass("disabled");
                    $('#exportButton').parent().removeClass("loadingPointer");

                    setTokenExpirationListener(timetoExpire, 5);
                },

                (error) => {

                    console.log("error in api");

                }
            )


        function setTokenExpirationListener(tokenExpiration,
            minutesToRefresh) {
            // get current time
            var currentTime = Date.now();
            var expiration = Date.parse(tokenExpiration);
            var safetyInterval = minutesToRefresh * 60 * 1000;
            // time until token refresh in milliseconds
            var timeout = expiration - currentTime - safetyInterval;
            // if token already expired, generate new token and set the access token
            if (timeout <= 0) {
                console.log("Updating Report Embed Token");
                updateToken();
            }
            // set timeout so minutesToRefresh minutes before token expires, token will be updated
            else {
                console.log("Report Embed Token will be updated in " + timeout / 60000 + " minutes.");
                setTimeout(function () {
                    updateToken();
                }, timeout);
            }
        }

        function updateToken() {
            // Generate new EmbedToken
            var request = fetch(apiEndPoint)
                .then(res => res.json())
                .then(
                    (result) => {
                        // Read embed application token from Model
                        var accessToken = result.EmbedToken.token;
                        // Read report Id from Model
                        //var embedReportId = result.Id;
                        // Read embed URL from Model
                        var embedUrl = result.EmbedUrl;
                        config["embedUrl"] = embedUrl;
                        // Read timetoExpire from Model
                        var tokenExpiry = result.EmbedToken.expiration;
                        window.report = powerbi.get(reportContainer);
                        report.setAccessToken(accessToken);
                        setTokenExpirationListener(tokenExpiry, 5 /*minutes before expiration*/);
                        // Clear any other error handler event
                        report.off("error");
                        // Below patch of code is for handling errors that occur during embedding
                        report.on("error", function (event) {
                            var errorMsg = event.detail;
                            // Use errorMsg variable to log error in any destination of choice
                            console.error(errorMsg);
                            return;
                        });
                        // Report.off removes a given event handler if it exists. 
                        report.off("loaded");

                        // Report.on will add an event handler which prints to Log window.
                        report.on("loaded", function () {
                            // Set token expiration listener

                            setTokenExpirationListener(tokenExpiry,
                                5 /*minutes before expiration*/);
                        });
                    },
                    (error) => {
                        console.error("Error on server side\n" + JSON.stringify(err));
                        document.write(err.responseText + '<br/>' + 'Check Admin logs or contact MSFT support for further details.');
                    }
                )
        }

    }


    componentDidUpdate() {

        this.callPowerBI();

    }

    componentDidMount() {



        this.callPowerBI();


    }

    render() {

        var element = document.getElementById('embedContainer');
        if (typeof (element) != 'undefined' && element != null) {
            powerbi.reset($('#embedContainer')[0]);
        }

        return (
            <div id="embedContainer" className="reportPlaceholder">

            </div>
        );
    }
}

export default RenderReport;