﻿import React, { Component } from "react";
import Dropdown from 'react-bootstrap/Dropdown'
import './landingpage.css'
import RenderReport from '../report/report'

class LandinPage extends React.Component {

    render() {


        return (
            <div className="report-content">
                <RenderReport report={this.props.report}></RenderReport>
            </div>
        );
    }
}




class SideBar extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            reportData: [],

            currentReportID: 0,
            reportListLoaded: false
        }
        this.accessToken = "";
        this.groupId = "a464fc26-2cb1-40c7-b03d-f39ca678a476";
        this.reportId = "";
        this.fileName = ""; // This is the file name that you want for your exported file	
        this.fileFormat = "PDF"
        this.exportConfig = {};
        this.exportConfig.format = this.fileFormat; // PDF, PPTX, PNG for general PBI reports (non-paginated)	
        this.report;
        this.bookmark = null;
        this.exportId;
        this.exportStatus = false;
        this.renderReport = this.renderReport.bind(this);
        this.Role = null;
        this.dataSetID = null;

    }




    componentWillMount() {

        fetch("Home/GetReports")
            .then(res => res.json())
            .then(
                (result) => {

                    this.setState(
                        {
                            reportData: result
                            ,
                            reportListLoaded: true
                        }
                    );


                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {

                    console.log("Error in API. Couldn't fetch the list of reports in the workspace. ");

                }
            )
    }



    renderReport(id) {

        this.setState({
            currentReportID: id
        }
        );

    }



    startExport() {
        $('#exportButton').addClass("disabled");
        $('#exportButton').parent().addClass("loadingPointer");

        let apiEndPoint = "/Home/GetToken/";
        var parent = this;
        var request = fetch(apiEndPoint)
            .then(res => res.json())
            .then(
                (result) => {

                    parent.accessToken = result;
                    window.report.bookmarksManager.capture().then(function (capturedBookmark) {

                        parent.bookmark = capturedBookmark.state;
             

                        //this.accessToken = result;
                        parent.exportAPI();
                    });


                },

                (error) => {

                    console.log("error in api");

                }
            )

    }




      exportAPI() {
      

          var groupId = this.groupId;
          var reportId = this.reportId;
          var accessToken = this.accessToken;
          var fileName = this.fileName;
          var exportConfig = this.exportConfig;

          var PowerBIReportExportConfiguration = {};

          var PageBookMark = {
              state: this.bookmark
          }

          if (this.Role === "Analyst") {

              PowerBIReportExportConfiguration = {

                  defaultBookmark: null,
                  identities: [
                      {
                          username: sessionStorage.getItem("userName"),
                          roles: [this.Role],
                          datasets: [this.dataSetID]
                      }
                  ]
              };

            
          }
          PowerBIReportExportConfiguration.defaultBookmark = PageBookMark;
          exportConfig.powerBIReportConfiguration = PowerBIReportExportConfiguration


        


        $.ajax({
            method: "POST",
            url: 'https://api.powerbi.com/v1.0/myorg/groups/' + this.groupId + '/reports/' + this.reportId + '/ExportTo',
            headers: {
                Authorization: 'Bearer ' + accessToken
            },
            dataType: "json",
            data: JSON.stringify(exportConfig),
            contentType: "application/json",
            success: function (response) {
                this.exportId = response.id;
                var exportId = this.exportId;
 
                checkStatus(groupId, reportId, exportId, accessToken, fileName);
            },
            error: function (error) {
                console.log(error);
            }
        });



          // API call #2 - Polling - check status	
          function checkStatus(groupId, reportId, exportId, accessToken, fileName) {
 
              var timeInterval = 2000; // Time interval for status check	

              $.ajax({
                  method: "GET",
                  url: 'https://api.powerbi.com/v1.0/myorg/groups/' + groupId + '/reports/' + reportId + '/exports/' + exportId,
                  headers: {
                      Authorization: 'Bearer ' + accessToken
                  },
                  success: function (response) {
                      console.log(response.status + ': ' + response.percentComplete + '%');
                      if (response.status !== "Succeeded") {
                          setTimeout(function () {
                              checkStatus(groupId, reportId, exportId, accessToken, fileName);
                          }, timeInterval);
                      } else {
                          downloadFile(groupId, reportId, exportId, accessToken, fileName);
                      }
                  },
                  error: function (error) {
                      console.log(error);
                  }
              });
          }




          // API call #3 - Download the file	
          function downloadFile(groupId, reportId, exportId, accessToken, fileName) {
              console.log("Downloading the file....");
              $.ajax({
                  method: "GET",
                  url: 'https://api.powerbi.com/v1.0/myorg/groups/' + groupId + '/reports/' + reportId + '/exports/' + exportId + '/file',
                  headers: {
                      Authorization: 'Bearer ' + accessToken
                  },
                  xhrFields: {
                      responseType: 'arraybuffer'
                  },
                  success: function (response, textStatus, request) {

                      //$('.loader').addClass('hidden');	
                      var blob = new Blob([response], { type: request.getResponseHeader('Content-Type') });
                      var URL = window.URL || window.webkitURL;
                      var dataUrl = URL.createObjectURL(blob);
                      var element = document.createElement('a');
                      element.setAttribute('href', dataUrl);
                      element.setAttribute('download', fileName);
                      element.style.display = 'none';
                      document.body.appendChild(element);
                      element.click();
                      $('#exportButton').removeClass("disabled");
                      $('#exportButton').parent().removeClass("loadingPointer");

                  },
                  error: function (error) {
                      console.log(error);
                  }
              })
          }



    }


    render() {

        var reportList = null;
        if (this.state.reportListLoaded) {


            reportList = this.state.reportData.value.map((element, id) => {

                if (this.state.currentReportID === id) {
                    this.reportId = element.id;
                    this.fileName = element.name;
                    this.Role = element.Role;
                    this.dataSetID = element.datasetId;
                    return (<li id="sidebarReportListItem">
                        <a key={id} title={element.name} className="active" onClick={() => this.renderReport(id)}>{element.name}</a>
                    </li>
                    );
                }
                else {
                    return (<li id="sidebarReportListItem">
                        <a key={id} title={element.name} onClick={() => this.renderReport(id)}>{element.name}</a>
                    </li>
                    );
                }

            });


        } else {
            reportList = <div class='loader-small'><div></div></div>;

        }

        return (

            <div>
                <div className="sidebar">
                    <div className="reportSectionHeader"><span id="reportsLogo"><img src="Scripts/react/react-components/landingPage/news-admin.png" /></span><span id="reportsText"> Reports </span></div>
                    <ul className="sidebarReportList">
                        {reportList}
                    </ul>
                    <div className="exportButton"><button className="btn btn-light btn-sm btn-block" id="exportButton" onClick={()=>this.startExport()}>Export To PDF</button></div>
                </div>
                {
                    this.state.reportListLoaded ?
                        <LandinPage report={this.state.reportData.value[this.state.currentReportID].name}></LandinPage>
                        : <div>Report will be rendered here</div>
                }


            </div>



        );
    }
}
export { LandinPage, SideBar };